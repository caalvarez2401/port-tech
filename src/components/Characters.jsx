import { useState, useEffect} from 'react'
import axios from "axios";
import './styles/Characters.css'
import { Character } from './Character';

const API_MARVEL = 'https://gateway.marvel.com:443/v1/public/characters?ts=1&apikey=7efd5cb48ca0760362b4ee411273f131&hash=94ac623f34aa0301edb670cd4df32d13'

const Characters = () => {
   const [currentPage, setCurrentPage] = useState(1);
   const [itemsPerPage, setItemsPerPage] = useState(5);
   const [characters, setCharacters] = useState([]);
   const [text, setText] = useState('');

   const handleClick = (e) => {
      setCurrentPage(Number(e.target.id))
   }

   useEffect(() => {
       axios.get(API_MARVEL)
      .then(res =>(
         setCharacters(res.data.data.results)
   ))
      .catch(error => console.log(error))
   }, [])
   

   const handleOnChange= (e) => {
      setText(e.target.value)
   }

   const filter = (val) => { 
      if(text === ""){
         return val
      }else if(val.name.toLowerCase().includes(text.toLowerCase())){
         return val
      }
    }

   const pages = []
   for(let i = 1; i <= Math.ceil(characters.length/itemsPerPage); i++){
      pages.push(i)
   }

   
   const renderPageNumbers = pages.map((number)=>{
      return (
         <li key={number} id={number} onClick={handleClick}>
            {number}
         </li>
      )
   })

   const indexOfLastItem = currentPage*itemsPerPage;
   const indexOfFirstItem = indexOfLastItem - itemsPerPage;
   const currentItems = characters.slice(indexOfFirstItem, indexOfLastItem)

console.log(currentItems);
   return(
      <div>
         <div className="container-input">
            <input 
               type="text"
               placeholder="Buscar ..."
               className="input" 
               value={text}
               onChange={handleOnChange}  
            />
         </div>

         

         <div className="row row-cols-1 row-cols-md-3 g-4 items-marvel" style={{display: 'flex', justifyContent: 'center', marginBottom: '100px'}}>
            
            {
               currentItems && currentItems.filter(filter).map((character) => (
                  <Character key={character.id} character={character}/>
               ))
            }
            
         </div>

         <ul className="PageNumbers">
            {renderPageNumbers}
         </ul>

      </div>
   )
}

export {Characters}