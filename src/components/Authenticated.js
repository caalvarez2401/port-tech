import '../App.css';

import { useAuth0 } from "@auth0/auth0-react";
import { LoginButton } from './Login'
import {LogoutButton } from './Logout'
import {Profile} from './Profile'

function Authenticated() {
  const {isAuthenticated} = useAuth0();
  return (
    <div className="App">
      <header className="App-header">
        {
          isAuthenticated ? <>
              <LogoutButton />
              <Profile />
            </>
          : <LoginButton />
        }
        
      </header>
    </div>
  );
}

export default Authenticated;
