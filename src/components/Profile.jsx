import { useAuth0 } from "@auth0/auth0-react";
import './styles/Profile.css'
import {Characters} from './Characters'


const Profile = () => {
   const {user} = useAuth0();

   return (
      
         <div>
            <div className="Profile">
               <div className="profile-name">
                  <h2>Hola, {user.given_name}</h2>
               </div> 
            </div>


            <Characters/>

         </div>
      )

}

export {Profile};