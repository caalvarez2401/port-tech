import {Accordion} from 'react-bootstrap'
import {useState} from 'react'
import { Button, Modal } from 'react-bootstrap';
import './styles/Character.css'

const Character = ({character}) => {
   const [show, setShow] = useState(false);

   const handleClose = () => setShow(false);
   const handleShow = () => setShow(true);

   return(
      <div className="">
         <Accordion  className="Character-Accordion" defaultActiveKey="0" style={{width: '200px', height: '200px', marginRight: '20px', marginBottom: '70px'}}>
            <Accordion.Item style={{backgroundColor: 'white'}} eventKey="0">
               <Accordion.Header>
                  <img className="card-img-top" src={`${character.thumbnail.path}.${character.thumbnail.extension}`} alt={character.name} style={{width: '186px', height: '200px', objectFit: 'cover'}}/>
               </Accordion.Header>
               <Accordion.Body >
                  <Button variant="primary" onClick={handleShow} className="Modal-Button">
                     {character.name}
                  </Button>
               </Accordion.Body>
            </Accordion.Item>
         </Accordion>

         <Modal show={show} onHide={handleClose} animation={false}>
            <Modal.Header closeButton>
               <Modal.Title>{character.name}</Modal.Title>
            </Modal.Header>
            <Modal.Body style={{color: 'black', fontSize: '12px'}}>{character.description}</Modal.Body>
            <Modal.Body style={{color: 'black', fontSize: '12px'}}>Ha estado en : {character.comics.available} comics</Modal.Body>
            <Modal.Body style={{color: 'black', fontSize: '12px'}}>Los comics son: {character.comics.items.map(item => <ul><li>{item.name}</li></ul>)}</Modal.Body>
            <Modal.Body style={{color: 'black', fontSize: '12px'}}>Ha estado en: {character.events.available} eventos</Modal.Body>
            <Modal.Body style={{color: 'black', fontSize: '12px'}}>
               Los eventos son: {character.events.items.map(item => (<ul><li>{item.name}</li></ul>))}
            </Modal.Body>

            <Modal.Footer>
               <Button variant="secondary" onClick={handleClose}>
                  Close
               </Button>
            </Modal.Footer>
         </Modal>
         
      </div>
   )
}

export {Character}