import React from 'react';
import './styles/Login.css'
import { useAuth0 } from '@auth0/auth0-react';

const LoginButton = () => {
   const {loginWithRedirect} = useAuth0();
   
   return (
      <div className="Login">
         <div className="Login-Image">
            <img src='https://as01.epimg.net/meristation/imagenes/2021/12/09/noticias/1639047639_886884_1639048122_noticia_normal.jpg' alt="" />
         </div>
         <button onClick={() => loginWithRedirect()}>Login</button>
      </div>
   )
}

export {LoginButton};