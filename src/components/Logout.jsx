import { useAuth0 } from "@auth0/auth0-react";
import './styles/Logout.css'

const LogoutButton = () => {
   const { logout } = useAuth0();

   return (
      <div className="Logout">
         <button className="Logout-button" onClick={() => logout({returnTo: window.location.origin})}>Logout</button>
      </div>
   )
}

export {LogoutButton};