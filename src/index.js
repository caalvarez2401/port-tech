import React from 'react';
import ReactDOM from 'react-dom';
import {Auth0Provider} from '@auth0/auth0-react'
import './index.css';
import App from './routes/App';

ReactDOM.render(
    <Auth0Provider 
      domain="dev-uaz217lr.us.auth0.com" 
      clientId="QozdQFGHVtWi6BKglp03iqPQOVIod1y5" 
      redirectUri={window.location.origin}
    >
      <App />
    </Auth0Provider>,
  document.getElementById('root')
);
