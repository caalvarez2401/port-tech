import {
  BrowserRouter,
  Switch,
  Route
} from "react-router-dom";
import '../App.css';
import { Characters } from '../components/Characters';
import Authenticated from '../components/Authenticated'


const App = () => {
   return (
      <BrowserRouter>
         <Switch>
            <Route path="/" component={Authenticated}/>
            <Route exact path="/ " component={Characters} />
         </Switch>
      </BrowserRouter>
   )
}

export default App;